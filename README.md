# Nik's Hütte 2019

Poster for the VCS hosting of Nik's Hütte 2019.

## Compiling

This poster was compiled using the TeXlive distribution under Arch Linux, using the luaLaTeX compiler.
